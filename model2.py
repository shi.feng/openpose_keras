from keras.initializers import random_normal, constant
from keras.layers import Activation, Input, Lambda
from keras.layers.convolutional import Conv2D
from keras.layers.merge import Concatenate
from keras.layers.merge import Multiply
from keras.layers.pooling import MaxPooling2D
from keras.models import Model
from keras.regularizers import l2


def relu(x): return Activation('relu')(x)


def conv2d(tensor, nf, ks, name, weight_decay):
    kernel_reg = l2(weight_decay[0]) if weight_decay else None
    bias_reg = l2(weight_decay[1]) if weight_decay else None

    tensor = Conv2D(nf, (ks, ks), padding='same', name=name,
                    kernel_regularizer=kernel_reg,
                    bias_regularizer=bias_reg,
                    kernel_initializer=random_normal(stddev=0.01),
                    bias_initializer=constant(0.0))(tensor)
    return tensor


def pooling(tensor, ks, st, name):
    tensor = MaxPooling2D((ks, ks), strides=(st, st), name=name)(tensor)
    return tensor


def vgg_module(tensor, weight_decay, net_info):
    """

    :param tensor: union
    :param weight_decay: list
    :param net_info: dict
    :return: union
    """
    # Block 1
    net = conv2d(tensor, net_info['conv1_1']['weights'][3], 3, "conv1_1", (weight_decay, 0))
    net = relu(net)
    net = conv2d(net, net_info['conv1_2']['weights'][3], 3, "conv1_2", (weight_decay, 0))
    net = relu(net)
    net = pooling(net, 2, 2, "pool1_1")

    # Block 2
    net = conv2d(net, net_info['conv2_1']['weights'][3], 3, "conv2_1", (weight_decay, 0))
    net = relu(net)
    net = conv2d(net, net_info['conv2_2']['weights'][3], 3, "conv2_2", (weight_decay, 0))
    net = relu(net)
    net = pooling(net, 2, 2, "pool2_1")

    # Block 3
    net = conv2d(net, net_info['conv3_1']['weights'][3], 3, "conv3_1", (weight_decay, 0))
    net = relu(net)
    net = conv2d(net, net_info['conv3_2']['weights'][3], 3, "conv3_2", (weight_decay, 0))
    net = relu(net)
    net = conv2d(net, net_info['conv3_3']['weights'][3], 3, "conv3_3", (weight_decay, 0))
    net = relu(net)
    net = conv2d(net, net_info['conv3_4']['weights'][3], 3, "conv3_4", (weight_decay, 0))
    net = relu(net)
    net = pooling(net, 2, 2, "pool3_1")

    # Block 4
    net = conv2d(net, net_info['conv4_1']['weights'][3], 3, "conv4_1", (weight_decay, 0))
    net = relu(net)
    net = conv2d(net, net_info['conv4_2']['weights'][3], 3, "conv4_2", (weight_decay, 0))
    net = relu(net)

    # Additional non vgg layers
    net = conv2d(net, net_info['conv4_3_CPM']['weights'][3], 3, "conv4_3_CPM", (weight_decay, 0))
    net = relu(net)
    net = conv2d(net, net_info['conv4_4_CPM']['weights'][3], 3, "conv4_4_CPM", (weight_decay, 0))
    net = relu(net)

    return net


def stage_1_module(net, branch, weight_decay=None, net_info=None):
    if net_info is None:
        raise ValueError('net_info is None')
    for i in range(1, 6):
        layer_name = "conv5_{layer}_CPM_L{branch}".format(layer=i, branch=branch)
        net = conv2d(net,
                     net_info[layer_name]['weights'][3],
                     net_info[layer_name]['weights'][0],
                     layer_name,
                     (weight_decay, 0))
        if i != 5:
            net = relu(net)

    return net


def stage_gte2_module(net, stage, branch, weight_decay=None, net_info=None):
    if net_info is None:
        raise ValueError('net_info is None')

    for i in range(1, 8):
        layer_name = "Mconv{layer}_stage{stage}_L{branch}".format(layer=i, stage=stage, branch=branch)
        net = conv2d(net,
                     net_info[layer_name]['weights'][3],
                     net_info[layer_name]['weights'][0],
                     layer_name,
                     (weight_decay, 0))
        if i != 7:
            net = relu(net)

    return net


def apply_mask(x, mask1, mask2, num_p, stage, branch):
    w_name = "weight_stage%d_L%d" % (stage, branch)
    if num_p == 38:
        w = Multiply(name=w_name)([x, mask1])  # vec_weight

    else:
        w = Multiply(name=w_name)([x, mask2])  # vec_heat
    return w


def get_training_model(weight_decay, net_info=None):
    if net_info is None:
        raise ValueError('net_info is None')

    stages = 6
    np_branch1 = 38
    np_branch2 = 19

    img_input_shape = (None, None, 3)
    vec_input_shape = (None, None, 38)
    heat_input_shape = (None, None, 19)

    inputs = []
    outputs = []

    img_input = Input(shape=img_input_shape)
    vec_weight_input = Input(shape=vec_input_shape)
    heat_weight_input = Input(shape=heat_input_shape)

    inputs.append(img_input)
    inputs.append(vec_weight_input)
    inputs.append(heat_weight_input)

    img_normalized = Lambda(lambda x: x / 256 - 0.5)(img_input)  # [-0.5, 0.5]

    # VGG
    stage0_out = vgg_module(img_normalized,
                            weight_decay,
                            net_info=net_info)

    # stage 1 - branch 1 (PAF)
    stage1_branch1_out = stage_1_module(stage0_out,
                                        1,
                                        weight_decay,
                                        net_info=net_info)
    w1 = apply_mask(stage1_branch1_out, vec_weight_input, heat_weight_input, np_branch1, 1, 1)

    # stage 1 - branch 2 (confidence maps)
    stage1_branch2_out = stage_1_module(stage0_out,
                                        2,
                                        weight_decay,
                                        net_info=net_info)
    w2 = apply_mask(stage1_branch2_out, vec_weight_input, heat_weight_input, np_branch2, 1, 2)

    x = Concatenate()([stage1_branch1_out, stage1_branch2_out, stage0_out])

    outputs.append(w1)
    outputs.append(w2)

    # stage sn >= 2
    for sn in range(2, stages + 1):
        # stage SN - branch 1 (PAF)
        stage_gte2_branch1_out = stage_gte2_module(x, sn, 1, weight_decay, net_info)
        w1 = apply_mask(stage_gte2_branch1_out, vec_weight_input, heat_weight_input, np_branch1, sn, 1)

        # stage SN - branch 2 (confidence maps)
        stage_gte2_branch2_out = stage_gte2_module(x, sn, 2, weight_decay, net_info)
        w2 = apply_mask(stage_gte2_branch2_out, vec_weight_input, heat_weight_input, np_branch2, sn, 2)

        outputs.append(w1)
        outputs.append(w2)

        if sn < stages:
            x = Concatenate()([stage_gte2_branch1_out, stage_gte2_branch2_out, stage0_out])

    model = Model(inputs=inputs, outputs=outputs)

    return model


def get_testing_model(net_info):
    stages = 6

    img_input_shape = (None, None, 3)

    img_input = Input(shape=img_input_shape)

    img_normalized = Lambda(lambda x: x / 256 - 0.5)(img_input)  # [-0.5, 0.5]

    # VGG
    stage0_out = vgg_module(img_normalized, None, net_info=net_info)

    # stage 1 - branch 1 (PAF)
    stage1_branch1_out = stage_1_module(stage0_out, 1, None, net_info=net_info)

    # stage 1 - branch 2 (confidence maps)
    stage1_branch2_out = stage_1_module(stage0_out, 2, None, net_info=net_info)

    x = Concatenate()([stage1_branch1_out, stage1_branch2_out, stage0_out])

    # stage t >= 2
    stage_gte2_branch1_out = None
    stage_gte2_branch2_out = None
    for sn in range(2, stages + 1):
        stage_gte2_branch1_out = stage_gte2_module(x, sn, 1, None, net_info=net_info)
        stage_gte2_branch2_out = stage_gte2_module(x, sn, 2, None, net_info=net_info)

        if sn < stages:
            x = Concatenate()([stage_gte2_branch1_out, stage_gte2_branch2_out, stage0_out])

    model = Model(inputs=[img_input], outputs=[stage_gte2_branch1_out, stage_gte2_branch2_out])

    return model
