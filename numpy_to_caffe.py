import os

import numpy as np

import caffe
from util import get_net_info

# KERAS_LAYERS_DIR = 'keras/layers'
CAFFE_DIR = 'model/caffe/_trained_COCO'

compress_rate = 70
caffe_proto_filename = 'pose_pruned_{percent}p.prototxt'.format(percent=compress_rate)
caffe_model_filename = 'pose_pruned_{percent}p.caffemodel'.format(percent=compress_rate)
caffe_proto = os.path.join(CAFFE_DIR, caffe_proto_filename)
caffe.set_mode_cpu()
net = caffe.Net(caffe_proto, caffe.TEST)

net_weights = np.load('model/openpose_pruned_{percent}p.npy'.format(percent=compress_rate),
                      encoding='bytes').item()

net_info = get_net_info(net_weights)
# net_weights = sorted(net_weights.items(), key=operator.itemgetter(0))

for name, param in net.params.items():
    # Set all weights and biases from Caffe model into Keras model
    if name in net_weights:
        w = net_weights[name][b'weights']
        b = net_weights[name][b'biases']
        # Tranfer the right form to store
        w = np.transpose(w, (3, 2, 0, 1))
        # Store into Keras model
        net.params[name][0].data[...] = w
        net.params[name][1].data[...] = b

# Output Caffe model
caffe_model = os.path.join(CAFFE_DIR, caffe_model_filename)
net.save(caffe_model)

print('-------------------------Finished converting---------------------------')
