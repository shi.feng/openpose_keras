import argparse

import numpy as np
from google.protobuf import text_format

import caffe
from caffe.proto import caffe_pb2

"""
 1. num_chl_in:
 2. num_chl_out: this value is equal to number of filters in the layer
 3. row_fi:
 4. col_fi:
 5. row_fo:
 6. col_fo:
 7. kn_size:
 8. kn_stride:
 9. padding:
10. mode:
11. addr_in:
12. addr_weight:
13. addr_out:
14. flag_pool:
15. flag_relu:
16. addr_bias:
"""
arg_str = ['num_chl_in', 'num_chl_out',
           'row_fi', 'col_fi', 'row_fo', 'col_fo',
           'kn_size', 'kn_stride', 'padding',
           'mode',
           'addr_in', 'addr_weight', 'addr_out',
           'flag_pool', 'flag_relu',
           'addr_bias']


def collect_net_info(proto):
    net_info = caffe_pb2.NetParameter()
    text_format.Merge(open(proto).read(), net_info)

    caffe.set_mode_cpu()
    caffe_net = caffe.Net(proto, caffe.TEST)
    fo_shapes = {k: v.data.shape for k, v in caffe_net.blobs.items()}
    pm_shapes = {k: [v[0].data.shape, v[1].data.shape] for k, v in caffe_net.params.items()}

    net_conf = {}
    address = 0
    for layer in net_info.layer:
        if layer.type == 'Convolution':
            if not (layer.name in net_conf):
                net_conf[layer.name] = {}
            param = layer.convolution_param
            net_conf[layer.name]['name'] = layer.name
            net_conf[layer.name]['mode'] = 1
            net_conf[layer.name]['num_chl_in'] = fo_shapes[layer.name][1]
            net_conf[layer.name]['num_chl_out'] = pm_shapes[layer.name][0][0]
            net_conf[layer.name]['row_fi'] = fo_shapes[layer.bottom[0]][2]
            net_conf[layer.name]['col_fi'] = fo_shapes[layer.bottom[0]][3]
            net_conf[layer.name]['row_fo'] = fo_shapes[layer.name][2]
            net_conf[layer.name]['col_fo'] = fo_shapes[layer.name][3]
            net_conf[layer.name]['kn_size'] = param.kernel_size[0] if len(param.kernel_size) else 1
            net_conf[layer.name]['kn_stride'] = param.stride[0] if len(param.stride) else 1
            net_conf[layer.name]['padding'] = param.pad[0] if len(param.pad) else 0
            net_conf[layer.name]['addr_in'] = 0
            net_conf[layer.name]['addr_out'] = 0
            memory_size = (int((np.prod(pm_shapes[layer.name][0]) +
                                np.prod(pm_shapes[layer.name][1])) >> 4) + 1) << 4
            net_conf[layer.name]['memory_size'] = memory_size
            address += memory_size
            net_conf[layer.name]['addr_weight'] = address
            # net_info[layer.name]['addr_bias'] = 0
            net_conf[layer.name]['flag_pool'] = 0
            net_conf[layer.name]['flag_relu'] = 0

        elif layer.type == 'Pooling':
            net_conf[layer.bottom[0]]['flag_pool'] = 1
        elif layer.type == 'ReLU':
            net_conf[layer.bottom[0]]['flag_relu'] = 1

    return net_conf


def mapping(net_conf, out):
    out_file = open(out, 'w')
    line = ''
    for count, (name, layer) in enumerate(net_conf.items()):
        line = "{count} " \
               "{layer_name} " \
               "{num_chl_in} " \
               "{num_chl_out} " \
               "{row_fi} " \
               "{col_fi} " \
               "{row_fo} " \
               "{col_fo} " \
               "{kn_size} " \
               "{kn_stride} " \
               "{padding} " \
               "{mode} " \
               "{addr_in} " \
               "{addr_out} " \
               "{addr_weight} " \
               "{flag_pool} " \
               "{flag_relu}\n".format(count=count,
                                      layer_name=layer.name,
                                      num_chl_in=layer['num_chl_in'],
                                      num_chl_out=layer['num_chl_out'],
                                      row_fi=layer['row_fi'],
                                      col_fi=layer['col_fi'],
                                      row_fo=layer['row_fo'],
                                      col_fo=layer['col_fo'],
                                      kn_size=layer['kn_size'],
                                      kn_stride=layer['kn_stride'],
                                      padding=layer['padding'],
                                      mode=layer['mode'],
                                      addr_in=0,
                                      addr_out=0,
                                      addr_weight=layer['addr_weight'],
                                      flag_pool=layer['flag_pool'],
                                      flag_relu=layer['flag_relu'])

        out_file.write(line)

    out_file.close()
    # return out


def parse_n_map(proto, out):
    net = collect_net_info(proto=proto)
    mapping(net, out=out)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Mapping caffe prototxt to Xilinx configuration')
    parser.add_argument('--proto', metavar='file', required=True,
                        help='the path to input prototxt file')
    parser.add_argument('--out', metavar='file', required=True,
                        help='the path to output file')
    args = parser.parse_args()
    parse_n_map(proto=parser.proto, out=parser.out)
